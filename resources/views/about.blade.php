@extends('master')

@push('style')
    
@endpush

@section('content')

    {{-- Menampilkan postingan  --}}
    <div class="container-fluid tags">
        <div class="row mt-3 mr-0">
            <div class="col-md-12">
                <div class="card card-primary">
                  <div class="card-header">
                    Tentang Kami
                  </div>  
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-4">
                        Nama aplikasi
                      </div>
                      <div class="col-md-8">
                        TweeThee
                      </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-md-4">
                        Framework
                      </div>
                      <div class="col-md-8">
                        <p>Laravel versi 6.*</p>
                      </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-md-4">
                        Template
                      </div>
                      <div class="col-md-8">
                        <p>AdminLTE-3.0.5</p>
                      </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-md-4">
                        Repository
                      </div>
                      <div class="col-md-8">
                        https://gitlab.com/arbu24/sosmed.git
                      </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-md-4">
                        Fungsionalitas aplikasi
                      </div>
                      <div class="col-md-8">
                        <ul>
                          <li>Autentikasi</li>
                          <li>Posting</li>
                          <li>Komentar</li>
                          <li>Pengaturan Profil</li>
                        </ul>
                      </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-md-4">
                        Deskripsi
                      </div>
                      <div class="col-md-8">
                        <p>Website ini merupakan tugas akhir dari pelatihan yang diadakan oleh Sanbercode. <br> Bootcamp Laravel Batch 22.</p> 
                      </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-md-4">
                        Kelompok 12
                      </div>
                      <div class="col-md-8">
                        <p>Anggota 1 : Arif Budiman</p>
                        <p>Anggota 2 : Joko Satriyo</p>
                        <p>Anggota 3 : Andy Rachman</p> 
                      </div>
                    </div>
                  </div>
                    <!-- /.card-body -->
                </div>
                <div class="card card-primary">
                  <div class="card-header">
                    Skema ERD
                  </div>  
                  <div class="card-body">
                    <div class="row">
                      <img src="{{asset('img/ERD/erd-sosmed.png')}}" width="90%" alt="skema-erd">
                    </div>
                  </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>
    {{-- akhir : menampilkan postingan --}}

@endsection

@push('script')

@endpush