@extends('master')

@push('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('assets-adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets-adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@endpush

@section('home_active')
    active
@endsection

@section('content')

    {{-- Menampilkan postingan  --}}
    <section class="content-header">
        <div class="container mb-2">
            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Edit Post</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="/post/{{$data->id}}" method="POST">
                        <div class="card-body">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                {{-- <label for="isi">Isi</label> --}}
                                <textarea class="form-control" name="isi" value="{{ old('isi','') }}" placeholder="Silahkan tulis disini ..." id="isi" rows="5">{{$data->isi}}</textarea>
                                @error('isi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            {{-- <div class="form-group">
                                <label for="judul">Tags</label>
                                <input type="text" class="form-control" name="tag" id="tag" value="{{ old('tag','') }}" placeholder="Masukan tags">
                                <p> Note : pisahkan dengan tanda koma ( , ) </p>
                                @error('tag')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div> --}}
                        </div>
                        <!-- /.card-body -->
                    
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a class = "btn btn-danger" href="{{route('post.index')}}">Back</a>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    {{-- akhir : menampilkan postingan --}}


@endsection

@push('script')

@endpush