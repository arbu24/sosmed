@extends('master')

@push('style')
  
@endpush

{{-- @section('home_active')
    active
@endsection --}}

@section('content')

    {{-- Menampilkan postingan  --}}
    <section class="content-header">
        <div class="container mb-2">
            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Detail Post</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="/post" method="POST">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <center><img src="{{ asset('img/'. $data->image) }}" width="max-width" height="350px"></center>
                                <hr>
                            </div>
                            <div class="form-group">
                                {!!$data->isi!!}
                                <hr>
                            </div>
                            <div class="form-group">
                                <label for="tag">Tags</label><br>
                                @forelse ($data->tags as $tag)
                                    <button class="btn btn-primary btn-sm">{{$tag->tag_name}}</button>
                                @empty
                                    <p> -Tidak Memiliki tags- </p>
                                @endforelse
                            </div>
                        </div>
                        <!-- /.card-body -->
                    
                        <div class="card-footer">
                            {{-- <button type="submit" class="btn btn-primary">Submit</button> --}}
                            <a class = "btn btn-danger" href="{{route('post.index')}}">Back</a>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    {{-- akhir : menampilkan postingan --}}


@endsection

@push('script')

@endpush