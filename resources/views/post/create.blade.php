@extends('master')

@push('style')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush


@section('content')

    {{-- Menampilkan postingan  --}}
    <section class="content-header">
        <div class="container mb-2">
            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Post</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="/post" method="POST" enctype="multipart/form-data">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                {{-- <label for="isi">Isi</label> --}}
                                {{-- <textarea class="form-control" name="isi"  placeholder="Silahkan tulis disini ..." id="isi" rows="5">{{ old('isi','') }}</textarea> --}}
                                <textarea name="isi" class="form-control my-editor">{!! old('isi', $isi ?? '') !!}</textarea>
                                @error('isi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="image">Gambar</label>
                                <input type="file" name="image" class="form-control" value="{{ old('image') }}">
                                {{-- @error('gambar')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror --}}
                                <p class="text-danger">{{ $errors->first('image') }}</p>
                            </div>
                            <div class="form-group">
                                <label for="tag">Tags</label>
                                <input type="text" class="form-control" name="tags" id="tag" value="{{ old('tag','') }}" placeholder="Masukan tags..">
                                <p> Note : pisahkan dengan tanda koma ( , ) </p>
                                @error('tag')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- /.card-body -->
                    
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a class = "btn btn-danger" href="{{route('post.index')}}">Back</a>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    {{-- akhir : menampilkan postingan --}}


@endsection

@push('script')
    <script>
        var editor_config = {
        path_absolute : "/",
        selector: "textarea.my-editor",
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
    
            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
            if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
            } else {
            cmsURL = cmsURL + "&type=Files";
            }
    
            tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
            });
        }
        };
    
        tinymce.init(editor_config);
    </script>
@endpush