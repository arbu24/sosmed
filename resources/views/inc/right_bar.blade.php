{{-- <section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        
    </div>
    </div><!-- /.container-fluid -->
</section> --}}

<div class="container-fluid tags">
    <div class="row mt-3 mr-2">
        <div class="col-md-12">
            <div class="card bg-light">
                <div class="card-header">
                  <h3 class="card-title"><i class="fas fa-tags"></i> Tags</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <a class="btn btn-primary btn-sm mb-2" href="#">Sambercode</a>
                    <a class="btn btn-primary btn-sm mb-2" href="#">Coding</a>
                    <a class="btn btn-primary btn-sm mb-2" href="#">Laravel</a>
                    <a class="btn btn-primary btn-sm mb-2" href="#">Bootstrap</a>
                    <a class="btn btn-primary btn-sm mb-2" href="#">AdminLTE</a>

                </div>
            </div>
        </div>
    </div>
    <div class="row mr-2">
        <div class="col-md-12">
            <div class="card bg-light">
                <div class="card-header">
                  <h3 class="card-title"><i class="fas fa-users"></i> Mungkin anda kenal?</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="user-panel mt-0 pb-0 mb-0">
                        <div class="row">
                            <div class="col-md-4">   
                                <div class="info">
                                    <img src="{{asset('assets-adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                                    <a href="#">Joko</a> 
                                    {{-- <a href="{{route('profile.show',['profile'=>$user->id])}}">{{$user->name}}</a> --}}
                                </div>
                            </div>
                            <div class="col-md-8">
                                <a class="btn btn-outline-primary btn-sm" href="#">Follow</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="user-panel mt-0 pb-0 mb-0">
                        <div class="row">
                            <div class="col-md-4">   
                                <div class="info">
                                    <img src="{{asset('assets-adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                                    <a href="#">Andy</a> 
                                    {{-- <a href="{{route('profile.show',['profile'=>$user->id])}}">{{$user->name}}</a> --}}
                                </div>
                            </div>
                            <div class="col-md-8">
                                <a class="btn btn-outline-primary btn-sm" href="#">Follow</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</div>