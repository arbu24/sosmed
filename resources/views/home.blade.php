@extends('master')

@push('style')
    
@endpush

@section('home_active')
    active
@endsection

@section('content')

    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
              <div class="col-sm-12">
              {{-- <h1>Home </h1> --}}
              <a class="btn btn-primary btn-lg btn-block" href="{{route('post.create')}}">TweeT</a>
              </div>
          </div>
        </div>
    </section>

    {{-- Menampilkan postingan  --}}
    <section>
      <div class="container-fluid mt-2">
        @if(session('success'))
            <div class="alert alert-success">
              {{session('success')}}
            </div>
        @endif
        <div class="row">
          @forelse ($data as $d)
          <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                  <div class="row">
                    <div class="col-md-9">
                      <div class="user-panel mt-1 pb-1 mb-1 d-flex">
                        <div class="image">
                            <img src="{{asset('assets-adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                        </div>
                        <div class="info">
                            <a href="#" class="d-block">{{ $d->user->name }}</a>
                            <p> {{$d->created_at}} </p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 d-flex flex-row-reverse">
                      @if ($d->user->id == Auth::id())
                        
                        <form action="{{route('post.destroy',['post'=>$d->id])}}" method="POST" style="display: inline-block">
                          @csrf
                          @method('DELETE')  
                          <a href="{{route('post.edit',[$post = $d->id])}}" type="button" class="btn btn-warning btn-sm"><i class="far fa-edit"></i></a>
                          <input type="submit" name="delete" id="delete" value=" x " class="btn btn-danger btn-sm">
                        </form>
                      @endif
                      <div class="mr-1" style="display: inline-block">
                        <a href="{{route('post.show',['post'=> $d->id])}}" type="button" class="btn btn-info btn-sm"><i class="far fas fa-eye"></i></a>
                      </div>                       
                    </div>
                  </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <center><img src="{{ asset('img/'. $d->image) }}" width="max-width" height="350px"></center>
                  <hr>
                  <p class=" mt-1">{!!$d->isi!!}</p>
                </div>
                <div class="card-footer">
                  <a href="" type="button" class="btn btn-primary btn-sm"><i class="fas fa-thumbs-up"></i></a>
                  <a href="" type="button" class="btn btn-primary btn-sm">comment <i class="far fa-comment"></i></a>
              </div>
            </div>
          </div>
          @empty
              <div class="col-md-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <p> Belum ada Postingan</p>
                </div>
            </div>
          </div>
          
          @endforelse
            
        </div>
      </div>
    </section>
    {{-- akhir : menampilkan postingan --}}

@endsection

@push('script')

@endpush