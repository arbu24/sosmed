@extends('master')

@push('style')
    
@endpush

@section('profile_active')
    active
@endsection

@section('content')

    {{-- <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
              <div class="col-sm-6">
              <h1>Home </h1>
              </div>
              <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="/">Home</a></li>
              </ol>
              </div>
          </div>
        </div>
    </section> --}}

    {{-- Menampilkan postingan  --}}
    <section class="content-header">
        <div class="container mb-2">
            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Edit Profile</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="/post" method="POST">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <label for="judul">Judul</label>
                                <input type="text" class="form-control" name="judul" id="judul" value="{{ old('judul','') }}" placeholder="Masukan Judul">
                                @error('judul')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="isi">Isi</label>
                                <textarea class="form-control" name="isi" value="{{ old('isi','') }}" placeholder="Silahkan tulis disini ..." id="isi" rows="5"></textarea>
                                @error('isi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- /.card-body -->
                    
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>
    {{-- akhir : menampilkan postingan --}}

@endsection

@push('script')

@endpush