@extends('master')

@push('style')
 
@endpush

@section('pengaturan_active')
    active
@endsection

@section('content')

    <section class="content-header">
        <div class="container mb-2">
            <div class="row mb-2">
                <div class="col-md-12">
                    <div class="card card-primary">
                        <div class="card-header">
                        <h3 class="card-title">Tambahkan Profile</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" action="/profile" method="POST" enctype="multipart/form-data">
                        <div class="card-body">
                            @csrf
                            <div class="form-group">
                                <label for="nama_lengkap">Nama Lengkap</label>
                                <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="{{ old('nama_lengkap','') }}" placeholder="Tulis nama lengkapmu disini ...">
                                @error('nama_lengkap')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="pekerjaan">Pekerjaan</label>
                                <input type="text" class="form-control" name="pekerjaan" id="pekerjaan" value="{{ old('pekerjaan','') }}" placeholder="Apa pekerjaan mu ?">
                                @error('pekerjaan')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="image">Photo</label>
                                <input type="file" name="image" class="form-control" value="{{ old('image') }}">
                                <p class="text-danger">{{ $errors->first('image') }}</p>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a class = "btn btn-danger" href="{{route('profile.index')}}">Back</a>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@push('script')

@endpush