@extends('master')

@push('style')
    
@endpush

@section('profile_active')
    active
@endsection

@section('content')

    {{-- <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
              <div class="col-sm-6">
              <h1>Home </h1>
              </div>
              <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="/">Home</a></li>
              </ol>
              </div>
          </div>
        </div>
    </section> --}}

    {{-- Menampilkan postingan  --}}
    <div class="container-fluid tags">
        <div class="row mt-3 mr-0">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                      <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                             src="{{asset('img/user/'.$query->photo)}}"
                             alt="User profile picture">
                      </div>
      
                      <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>
      
                      <p class="text-muted text-center">{{Auth::user()->email}}</p>
      
                      <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Nama Lengkap</b> <a class="float-right">{{$query->nama_lengkap}}</a>
                        </li>
                        <li class="list-group-item">
                          <b>Pekerjaan</b> <a class="float-right">{{$query->pekerjaan}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Bergabung Sejak</b> <a class="float-right">{{Auth::user()->created_at}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Followers</b> <a class="float-right">{{$query->follower}}</a>
                        </li>
                      </ul>
      
                      <a href="{{route('profile.edit',['profile'=>$query->id])}}" class="btn btn-warning btn-block"><b>Edit Profile</b></a>
                    </div>
                    <!-- /.card-body -->
                </div>
                {{-- <div class="card bg-light">
                    <div class="card-header">
                      <h3 class="card-title">Profile</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        photo :<br>
                        nama : {{$query->nama_lengkap}} <br>
                        Pekerjaan : {{$query->pekerjaan}}<br>
                        jumlah follower : {{$query->jumlah_follower}}
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    {{-- akhir : menampilkan postingan --}}

@endsection

@push('script')

@endpush