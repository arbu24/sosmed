@extends('master')

@push('style')
    
@endpush

@section('profile_active')
    active
@endsection

@section('content')

    {{-- Menampilkan postingan  --}}
    <div class="container-fluid tags">
        <div class="row mt-3 mr-0">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                      <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                             src="{{asset('img/user/'.$data->photo)}}"
                             alt="User profile picture">
                      </div>
      
                      <h3 class="profile-username text-center">{{$data->user->name}}</h3>
      
                      <p class="text-muted text-center">{{$data->user->email}}</p>
      
                      <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Nama Lengkap</b> <a class="float-right">{{$data->nama_lengkap}}</a>
                        </li>
                        <li class="list-group-item">
                          <b>Pekerjaan</b> <a class="float-right">{{$data->pekerjaan}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Bergabung Sejak</b> <a class="float-right">{{$data->user->created_at}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Followers</b> <a class="float-right">{{$data->follower}}</a>
                        </li>
                      </ul>
      
                      {{-- <a href="{{route('profile.edit',['profile'=>$query->id])}}" class="btn btn-warning btn-block"><b>Edit Profile</b></a> --}}
                      <a class="btn btn-danger" href="/home">Kembali</a>
                    </div>
                    <!-- /.card-body -->
                </div>
                {{-- <div class="card bg-light">
                    <div class="card-header">
                      <h3 class="card-title">Profile</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        photo :<br>
                        nama : {{$query->nama_lengkap}} <br>
                        Pekerjaan : {{$query->pekerjaan}}<br>
                        jumlah follower : {{$query->jumlah_follower}}
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
    {{-- akhir : menampilkan postingan --}}

@endsection

@push('script')

@endpush