@extends('master')

@push('style')
    
@endpush

@section('pengaturan_active')
    active
@endsection

@section('content')

    {{-- Menampilkan postingan  --}}
    <div class="container-fluid tags">
        <div class="row mt-3 mr-0">
            <div class="col-md-12">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                      <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle"
                             src="{{asset('assets-adminlte/dist/img/user2-160x160.jpg')}}"
                             alt="User profile picture">
                      </div>
      
                      <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>
      
                      <p class="text-muted text-center">{{Auth::user()->email}}</p>
      
                      <ul class="list-group list-group-unbordered mb-3">
                        <li class="list-group-item">
                            <b>Bergabung Sejak</b> <a class="float-right">{{Auth::user()->created_at}}</a>
                          </li>
                        <li class="list-group-item">
                          <b>Followers</b> <a class="float-right"></a>
                        </li>
                      </ul>
      
                      <a href="{{route('profile.create')}}" class="btn btn-primary btn-block"><b>Tambahkan Deskripsi</b></a>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>
    {{-- akhir : menampilkan postingan --}}

@endsection

@push('script')

@endpush