<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>TweeThee</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('assets-adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('assets-adminlte/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  {{-- <link rel="stylesheet" href="{{asset('assets-adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}"> --}}

  @stack('style')

</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  
  @include('inc.navbar')
  <!-- /.navbar -->

  
  <!-- Main Sidebar Container -->
  @include('inc.main_sidebar')
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <div class="row">
      <div class="col-md-8" style="">
        @yield('content')
      </div>
      <div class="col-md-4">
        @include('inc.right_bar')
      </div>
    </div>
    
  </div>
  <!-- /.content-wrapper -->

  {{-- @include('inc.footer') --}}

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('assets-adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('assets-adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets-adminlte/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('assets-adminlte/dist/js/demo.js')}}"></script>

{{-- <script src="{{asset('assets-adminlte/plugins/sweetalert2/sweetalert2.min.js')}}"></script> --}}

@include('sweetalert::alert')

@stack('script')

</body>
</html>
