<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
Use App\User;
use App\Profile;
use Auth;
Use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = Profile::find(Auth::id());
        // dd($query);
        return view('profile.index',compact('query'));
        // return view('profile.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "nama_lengkap"   => 'required',
            "pekerjaan" =>'required',
            "image" => 'required'
        ]);
        
        $file = $request->file('image');
        $filename = $file->getClientOriginalName();
        // dd($filename);
        $file->move(base_path('public/img/user/'), $filename);

        $data = Profile::create([
            'nama_lengkap'   => $request['nama_lengkap'],
            'pekerjaan' =>$request['pekerjaan'],
            'photo' => $filename,
            'user_id' => Auth::id() //cara 1 biasa
        ]);
        $user = Auth::user();
        $user->profile()->save($data);

        Alert::success('Berhasil', 'Profile Berhasil Disimpan');
        return redirect('/profile')->with('success','Profile Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Profile::find($id);
        // dd($query);
        return view('profile.edit',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Profile::find($id);
        // dd($query);
        return view('profile.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "nama_lengkap"   => 'required',
            "pekerjaan" =>'required',
            "image" => 'required'
        ]);

        $data = Profile::find($id);
        // dd($data);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $file->move(base_path('public/img/user/'), $filename);
            File::delete(base_path('public/img/user/' . $data->image));
        }
        // dd($filename);

        $data -> nama_lengkap = $request->nama_lengkap;
        $data -> pekerjaan = $request->pekerjaan;
        $data -> photo = $filename;
        $data -> user_id = Auth::id();
        $data ->update();

        Alert::success('Berhasil', 'Profile Berhasil Diupdate');
        return redirect('/profile')->with('success','Profile Berhasil di Edit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
