<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use App\Post;
use App\User;
Use Auth;
use App\Tag;
use File;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $data = $user->posts;

        // $data = Post::select('*')
        //                 ->orderBy("id", "desc")
        //                 ->get();
        return view('post.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "isi"   => 'required',
            "image" => 'required'
        ]);
        
        $file = $request->file('image');
        $filename = $file->getClientOriginalName();
        $file->move(base_path('public/img/'), $filename);

        // tags
        $tags_arr= explode(',',$request["tags"]);
        // dd($tags_arr);
        
        $tag_ids = [];
        foreach($tags_arr as $tag_name){
            $tag = Tag::firstOrCreate(['tag_name' => $tag_name]);
            $tag_ids[] = $tag->id;
            // $tag = Tag::where("tag_name", $tag_name)->first();
            // if($tag){
            //     $tag_ids[] = $tag->id;
            // } else {
            //     $new_tag = Tag::create(["tag_name" => $tag_name]);
            //     $tag_ids[] = $new_tag->id;
            // }
        }

        $post = Post::create([
            'isi'   => $request['isi'],
            'image' => $filename,
            'user_id' => Auth::id() //cara 1 biasa
        ]);

        $post->tags()->sync($tag_ids);

        $user = Auth::user();
        $user->posts()->save($post);

        // cara 2 method save
        // $user = Auth::user();
        // $user->posts()->save($post);

        // cara 3 method create
        // $user = Auth::user();
        // $post = $user->posts()->create([
        //     'isi'   => $request['isi']
        // ]);

        Alert::success('Berhasil', 'Post Berhasil Disimpan');

        return redirect('/post')->with('success','Post Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Post::find($id);
        return view('post.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Post::find($id);
        // $coba = $data->tags;

        // dd($coba);
        return view('post.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            "isi"   => 'required',
            "image" => 'required'
        ]);

        $data = Post::find($id);
        // dd($data);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $file->move(base_path('public/img/'), $filename);
            File::delete(base_path('public/img/' . $data->image));
        }
        // dd($filename);

        // $tags_arr= explode(',',$request["tags"]);
        
        // $tag_ids = [];
        // foreach($tags_arr as $tag_name){
        //     $tag = Tag::firstOrCreate(['tag_name' => $tag_name]);
        //     $tag_ids[] = $tag->id;
        // }
        
        // $data ->tags()->sync($tag_ids);
        
        $data -> isi = $request->isi;
        $data -> image = $filename;
        $data -> user_id = Auth::id();
        $data ->update();
        // dd($data);
        Alert::success('Berhasil', 'Post Berhasil Diupdate ');
        return redirect('/post')->with('success','Post Berhasil di Edit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Post::find($id);
        $data->delete();

        Alert::success('Berhasil', ' Post Berhasil Dihapus');            
        return redirect('/post')->with('success','Post Berhasil di Hapus!');
    }
}
